const axios = require('axios');
const chalk = require('chalk');

const getTransaction = async() => {
    try {
        const BITQUERY_HEADER = {
            headers: {
                'Content-Type': 'application/json',
                'X-API-KEY': 'BQYZJai0Ca7mPFakurCqCOPaqi7FxlaO'
            }
        };

        let oQuery = {
            query: `{
              ethereum(network: bsc) {
                transactions(
                  options: {limit: 10, desc: "nonce"}
                  txSender: {is: "0x4C074FFbA07750E355dCbD2764d0e130f6B303b5"}
                  txTo: {is: "0xeBC76079Da0c245faE7225b58a57A54809b40618"}
                ) {
                  nonce
                  success
                  hash
                }
              }
            }`,
            variables: {}
        };

        let _res = await axios.post('https://graphql.bitquery.io', oQuery, BITQUERY_HEADER);
        console.log("resukt===", _res.data.data.ethereum.transactions)
    } catch (err) {
        console.log("error==", err);
    }
}

const getTransactionCount = async(payer_address, contract_address) => {
    try {
        const BITQUERY_HEADER = {
            headers: {
                'Content-Type': 'application/json',
                'X-API-KEY': 'BQYZJai0Ca7mPFakurCqCOPaqi7FxlaO'
            }
        };

        let oQuery = {
            query: `{
              ethereum(network: bsc) {
                transactions(
                  txSender: {is: "0x4C074FFbA07750E355dCbD2764d0e130f6B303b5"}
                  txTo: {is: "0xeBC76079Da0c245faE7225b58a57A54809b40618"}
                ) {
                  count(txSender: {is: "0x4C074FFbA07750E355dCbD2764d0e130f6B303b5"})
                }
              }
            }`,
            variables: {}
        };

        let _res = await axios.post('https://graphql.bitquery.io', oQuery, BITQUERY_HEADER);
        console.log(chalk.green("Total transaction count ==="), _res.data.data.ethereum.transactions[0].count);
        return _res.data.data.ethereum.transactions;
    } catch (err) {
        console.log(chalk.red("error=="), err);
    }
};

//for testing purpose
async function test() {
    let requestResult;
    let requestIdQuery = `
                      SELECT id, request_id FROM payment_requests where request_id = '00389e3a-b38e-47f6-b78f-e6cea66afac8'
                      `;
    await con.query(requestIdQuery, function(err, result) {
        if (err) {
            console.log(chalk.red("Error == "), err);
            return;
        }
        console.log("Result of request id===", result[0].request_id);
        requestResult = result;
        console.log("Result of request id===", requestResult[0].request_id);
    });
}
getTransaction();
getTransactionCount()