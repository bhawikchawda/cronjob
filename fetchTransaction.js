var mysql = require('mysql');
const Web3 = require("web3");
const abiDecoder = require('abi-decoder');
const abi = require('./abi');
const axios = require('axios');
const chalk = require('chalk');
var web3 = new Web3("https://bsc-dataseed1.ninicoin.io");

//create connections
var con = mysql.createConnection({
    host: "bnbpay-dev.csjrjneiy6sm.ap-southeast-1.rds.amazonaws.com",
    user: "akshay",
    password: "at38.Bajv824djC",
    database: "bnbpay"
});

//check connections
const checkConnections = () => {
    con.connect(function(err) {
        if (err) {
            console.log("error==", err);
            return false;
        }
        console.log("Connected!");
        return true;
    });
}

//insert transaction
const fetchTransaction = () => {
    try {
        let sql = "SELECT * from payments WHERE status = 'pending'";
        con.query(sql, function(err, txnResult) {
            if (err) throw err;
            console.log("Result==", txnResult);
            if (txnResult.length == 0) {
                console.log(chalk.yellow("There are no pending transactions"));
                return;
            }
            fetchTxnOfAddress(txnResult);
        });
    } catch (err) {
        console.log("error==", err);
    }
};


//all txn of an particular address
const fetchTxnOfAddress = async(txnResult) => {
    try {
        let status;
        let allTransaction, iteration = 0;
        console.log("length===", txnResult.length);
        for (let counter = 0; counter < txnResult.length; counter++) {
            console.log("payer address===", txnResult[counter].payer_address);
            if (txnResult[counter].payer_address == null) continue;
            await axios.get(`https://api.bscscan.com/api?module=account&action=txlist&address=${txnResult[counter].payer_address}&startblock=0&endblock=99999999&page=1&offset=10&sort=asc&apikey=3BXDBYKK51QB45HZCYM5U8XQPDG8PR9YJP`)
                .then(function(response) {
                    if (response.status == 0) status = 0;
                    console.log("response==", response.data.result);
                    allTransaction = response.data.result;
                })
            if (status == 0) {
                console.log("No transaction is there....")
                continue;
            }
            for (let innerCounter of allTransaction) {
                iteration++;
                if (String(innerCounter.from).toUpperCase() === String(txnResult[counter].payer_address).toUpperCase() && String(innerCounter.to).toUpperCase() === String(txnResult[counter].settlement_contract_address).toUpperCase()) {
                    updateStatus(innerCounter.input, innerCounter.hash);

                } else {
                    console.log(`${innerCounter.from} ${txnResult[counter].payer_address}`);
                    console.log("payer address not matched");
                }
            }
            if (iteration == allTransaction.length) {
                console.log(chalk.red("....Wrong transaction id has assigned..."));
            }
        }

    } catch (err) {
        console.log(chalk.red("Error == "), err);
    }
};

//method for ABI decoder
abiDecoder.addABI(abi);

async function decodeData(code) {
    try {
        let code = '0x4000aea0000000000000000000000000239b6da6c784bc558dda1993c194cedd20e402580000000000000000000000000000000000000000000000000000000f36c9bc200000000000000000000000000000000000000000000000000000000000000060000000000000000000000000000000000000000000000000000000000000002431666532656464382d663639382d343964362d396365332d39363737646136636666303300000000000000000000000000000000000000000000000000000000';

        const decodedData = await abiDecoder.decodeMethod(code);

        console.log('decodedData', decodedData);


        let byteData = decodedData.params.find(obj => {
            return obj.type == 'bytes';
        });

        if (byteData) {
            let ascciBytes = web3.utils.toAscii(byteData.value);
            console.log('ascciBytes = ', ascciBytes);
            return ascciBytes;
        } else {
            console.log('no byte data found');
        }

        process.exit(0);
    } catch (error) {
        console.log(error)
    }
};


//update the status of transaction
const updateStatus = async(input, txnHash) => {
    let requestIdData = await decodeData(input);
    let requestResult;
    let requestIdQuery = `SELECT id,request_id FROM payment_requests where request_id = '${requestIdData}'`;
    con.query(requestIdQuery, function(err, result) {
        if (err) {
            console.log(chalk.red("Error == "), err);
            return;
        }
        console.log("Result of request id===", result);
        requestResult = result;
    });
    if (requestResult.length == 0) {
        console.log(chalk.red("......No request found....."));
        return;
    }
    console.log(chalk.green("....Updating The DATABASE ...."));
    console.log(`Updating the status to "captured" of payer address ${p_Address} with transaction hash ${txnHash}`);
    // var sql = `UPDATE payments SET status = 'captured' WHERE payer_address = '${p_Address}' AND bsc_txn_id = '${txnHash}'`;
    // con.query(sql, function(err, result) {
    //     if (err) {
    //         console.log(chalk.red("Error == "), err);
    //         return;
    //     }
    //     console.log(result.affectedRows + chalk.green("record(s) updated"));
    // });
};

//checkConnections();
//fetchTransaction();
decodeData();
// setInterval(() => {
//     fetchTransaction()
// }, 5000);