var mysql = require('mysql');
const Web3 = require("web3");
const abiDecoder = require('abi-decoder');
const abi = require('./abi');
const axios = require('axios');
const chalk = require('chalk');
var web3 = new Web3("https://bsc-dataseed1.ninicoin.io");

//create connections
// var con = mysql.createConnection({
//     host: "bnbpay-dev.csjrjneiy6sm.ap-southeast-1.rds.amazonaws.com",
//     user: "akshay",
//     password: "at38.Bajv824djC",
//     database: "bnbpay"
// });

var con = mysql.createConnection({
    host: "db4free.net",
    user: "yudizdev",
    password: "Admin@123",
    database: "bnbpay",
    port: 3306
});

//check connections
const checkConnections = () => {
    con.connect(function(err) {
        if (err) {
            console.log("error==", err);
            return false;
        }
        console.log("Connected!");
        return true;
    });
}

//insert transaction
const fetchTransaction = () => {
    try {
        let sql = "SELECT * from payments WHERE status = 'pending'";
        con.query(sql, function(err, txnResult) {
            if (err) throw err;
            console.log("Result==", txnResult);
            console.log("pending data == ", txnResult.length);
            if (txnResult.length == 0) {
                console.log(chalk.yellow("There are no pending transactions"));
                return;
            }
            findAllTransaction(txnResult);
        });
    } catch (err) {
        console.log("error in fetchTransaction==", err);
    }
};

const findAllTransaction = async(txnResult) => {
    let countercount = 0;
    let inner = 0
    try {
        for (let counter of txnResult) {
            console.log("outer counter == ", countercount += 1);
            if (counter.payer_address == null) continue;
            let totalTransactionCount = await getTransactionCount(counter.payer_address, counter.settlement_contract_address);
            let allSuccessTxn = await getAllTransaction(totalTransactionCount, counter.payer_address, counter.settlement_contract_address);
            for (let innerCounter of allSuccessTxn) {
                console.log("inner count ==", inner += 1);
                let inputData = await getInputData(innerCounter.hash);
                let requestId = await getRequestId(inputData);
                if (requestId == false) {
                    console.log(chalk.red("Not a valid function to find request id"));
                    continue;
                } else {
                    console.log("inside update....");
                    updateStatus(requestId, counter.payer_address, innerCounter.hash);
                }

            }
        }

    } catch (err) {
        console.log(chalk.red("error in findAllTransaction===", err));
    }
};

//method for finding input data
const getInputData = async(hash) => {
    try {
        let inputData = await web3.eth.getTransaction(hash);
        console.log(chalk.green('Input Data == '), inputData.input);
        return inputData.input;
    } catch (err) {
        console.log("error in getInputData==", err);
    }
};

//method for finding transaction count
const getTransactionCount = async(payer_address, contract_address) => {
    try {
        const BITQUERY_HEADER = {
            headers: {
                'Content-Type': 'application/json',
                'X-API-KEY': 'BQYZJai0Ca7mPFakurCqCOPaqi7FxlaO'
            }
        };

        let oQuery = {
            query: `{
                ethereum(network: bsc) {
                  transactions(
                    txSender: {is: "${payer_address}"}
                    txTo: {is: "${contract_address}"}
                  ) {
                    count(txSender: {is: "${payer_address}"})
                  }
                }
              }`

        };

        let _res = await axios.post('https://graphql.bitquery.io', oQuery, BITQUERY_HEADER);
        console.log(chalk.green("Total transaction count ==="), _res.data.data.ethereum.transactions[0].count);
        return _res.data.data.ethereum.transactions[0].count;
    } catch (err) {
        console.log(chalk.red("error=="), err);
    }
};

//method for getting all transaction

const getAllTransaction = async(limit, payer_address, contract_address) => {
    try {
        const BITQUERY_HEADER = {
            headers: {
                'Content-Type': 'application/json',
                'X-API-KEY': 'BQYZJai0Ca7mPFakurCqCOPaqi7FxlaO'
            }
        };

        let oQuery = {
            query: `{
                ethereum(network: bsc) {
                  transactions(
                    options: {limit: ${limit}, desc: "nonce"}
                    txSender: {is: "${payer_address}"}
                    txTo: {is: "${contract_address}"}
                  ) {
                    nonce
                    success
                    hash
                  }
                }
              }`

        };

        let _res = await axios.post('https://graphql.bitquery.io', oQuery, BITQUERY_HEADER);
        console.log(chalk.green(`Total transaction of payer ${payer_address} ===`), _res.data.data.ethereum.transactions);
        return _res.data.data.ethereum.transactions;
    } catch (err) {
        console.log(chalk.red("error=="), err);
    }
};


//getting request id
const getRequestId = async(inputData) => {

    try {
        abiDecoder.addABI(abi);
        const decodedData = await abiDecoder.decodeMethod(inputData);

        console.log('decodedData == ', decodedData);
        if (decodedData.name !== 'transferAndCall') {
            console.log(chalk.red("Invalid function call found..."));
            return false;
        }

        let byteData = decodedData.params.find(obj => {
            return obj.type == 'bytes';
        });

        if (byteData) {
            let ascciBytes = web3.utils.toAscii(byteData.value);
            console.log('ascciBytes = ', ascciBytes);
            return ascciBytes;
        } else {
            console.log(chalk.red('...There is no byte data...'));
            return false;
        }
    } catch (error) {
        console.log(chalk.red('Decoding error = '), error);
        return false;
    }
};

//update the status
const updateStatus = async(requestId, _payer_address, txnHash) => {
    try {
        let requestIdQuery = `SELECT id, request_id FROM payment_requests where request_id = '${requestId}'`;
        con.query(requestIdQuery, function(err, result) {
            if (err) {
                console.log(chalk.red("Error == "), err);
                return;
            }
            if (result.length == 0) {
                console.log(chalk.red("......No requestid found....."));
                return;
            }
            console.log("Result of request id===", result[0].request_id);
            console.log(chalk.green(`Updating the status to "captured" of request id = ${ result[0].request_id } with payer address = ${ _payer_address } and transaction hash = ${ txnHash }`));
            var sql = `UPDATE payments SET status = 'captured', payer_address = '${_payer_address}', bsc_txn_id = '${txnHash}'WHERE payment_request_id = '${result[0].id}'`;
            con.query(sql, function(err, resultUpdated) {
                if (err) {
                    console.log(chalk.red("Error == "), err);
                    return;
                }
                console.log(chalk.green("....Updating The DATABASE ...."));
                console.log(resultUpdated.message + chalk.green(" ..record(s) updated"));
            });
        });
    } catch (err) {
        console.log(chalk.red("Error in update status = "), err);
    }

};



checkConnections();
fetchTransaction();
// setInterval(() => {
//     fetchTransaction();
// },5000)
//test();