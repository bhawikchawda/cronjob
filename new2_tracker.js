var mysql = require('mysql');
const Web3 = require("web3");
const abiDecoder = require('abi-decoder');
const abi = require('./abi');
const axios = require('axios');
const chalk = require('chalk');
var web3 = new Web3("https://bsc-dataseed1.ninicoin.io");

//create connections
// var con = mysql.createConnection({
//     host: "bnbpay-dev.csjrjneiy6sm.ap-southeast-1.rds.amazonaws.com",
//     user: "akshay",
//     password: "at38.Bajv824djC",
//     database: "bnbpay"
// });

var con = mysql.createConnection({
    host: "db4free.net",
    user: "yudizdev",
    password: "Admin@123",
    database: "bnbpay",
    port: 3306
});

//check connections
const checkConnections = () => {
    con.connect(function(err) {
        if (err) {
            console.log("error==", err);
            return false;
        }
        console.log("Connected!");
        return true;
    });
}

//insert transaction
const fetchTransaction = () => {
    try {
        let sql = "SELECT * from payments WHERE payer_address = '0x91a6275327b42E77d24803ccC4d9682e748CEdd3' AND status = 'pending'";
        con.query(sql, function(err, txnResult) {
            if (err) throw err;
            console.log("Result==", txnResult);
            console.log("pending data == ", txnResult.length);
            if (txnResult.length == 0) {
                console.log(chalk.yellow("There are no pending transactions"));
                return;
            }
            findAllTransaction(txnResult);
        });
    } catch (err) {
        console.log("error in fetchTransaction==", err);
    }
};

const findAllTransaction = async(txnResult) => {
    let countercount = 0;
    let inner = 0
    try {
        for (let counter of txnResult) {
            console.log("outer counter == ", countercount += 1);
            if (counter.payer_address == null) continue;
            console.log("payer address==", counter.payer_address);
            let limit = await getTransactionCount(counter.payer_address, counter.settlement_contract_address);
            let allSuccessTxn = await getAllTransaction(limit, counter.payer_address, counter.settlement_contract_address);
            if (allSuccessTxn.length == 0) continue;
            for (let innerCounter of allSuccessTxn) {
                console.log("inner count ==", inner += 1);
                let inputData_ = innerCounter.arguments.find(o => o.argument === '_data');
                console.log("argument value==", inputData_.value);
                let requestId = await getRequestId(inputData_.value);
                if (requestId == false) {
                    console.log(chalk.red(chalk.red(`Error occured in finding request id of ${inputData_.value}`)));
                    continue;
                } else {
                    console.log("inside update....");
                    //updateStatus(requestId, counter.payer_address, innerCounter.hash);
                }

            }
        }

    } catch (err) {
        console.log(chalk.red("error in findAllTransaction===", err));
    }
};

//get transaction count
const getTransactionCount = async(payer_address, contract_address) => {
    try {
        const BITQUERY_HEADER = {
            headers: {
                'Content-Type': 'application/json',
                'X-API-KEY': 'BQYZJai0Ca7mPFakurCqCOPaqi7FxlaO'
            }
        };

        let oQuery = {
            query: `{
                ethereum(network: bsc) {
                  transactions(
                    txSender: {is: "${payer_address}"}
                    txTo: {is: "${contract_address}"}
                  ) {
                    count(txSender: {is: "${payer_address}"})
                  }
                }
              }`

        };

        let _res = await axios.post('https://graphql.bitquery.io', oQuery, BITQUERY_HEADER);
        console.log(chalk.green("Total transaction count ==="), _res.data.data.ethereum.transactions[0].count);
        return _res.data.data.ethereum.transactions[0].count;
    } catch (err) {
        console.log(chalk.red("error=="), err);
    }
};

//method for getting all transaction

const getAllTransaction = async(limit, payer_address, contract_address) => {
    try {
        const BITQUERY_HEADER = {
            headers: {
                'Content-Type': 'application/json',
                'X-API-KEY': 'BQYZJai0Ca7mPFakurCqCOPaqi7FxlaO'
            }
        };

        let oQuery = {
            query: `{
                ethereum(network: bsc) {
                  smartContractCalls(
                    caller: {is: "${payer_address}"}
                    smartContractAddress: {is: "${contract_address}"}
                    options: {limit: ${limit} desc: "block.timestamp.iso8601"}

                    smartContractMethod: {is: "transferAndCall"}
                    ) {
                    transaction {
                      hash
                    }
                    block {
                      timestamp {
                        iso8601
                      }
                    }
                    smartContractMethod {
                      name
                    }
                    arguments {
                      argument
                      value
                    }
                  }
                }
              }`

        };

        let _res = await axios.post('https://graphql.bitquery.io', oQuery, BITQUERY_HEADER);
        console.log(chalk.green(`Total transaction of payer ${payer_address}`), _res.data.data.ethereum.smartContractCalls[0].arguments);
        if (_res.data.data.ethereum.smartContractCalls.length == 0) {
            console.log(chalk.red("No transaction found...."))
            return _res.data.data.ethereum.smartContractCalls;
        }
        console.log(chalk.green(`Total transaction of payer ${payer_address} ===`), _res.data.data.ethereum.smartContractCalls.length);
        return _res.data.data.ethereum.smartContractCalls;
    } catch (err) {
        console.log(chalk.red("error=="), err);
    }
};

//finding request id
const getRequestId = async(inputData) => {
    try {
        let requestId = await web3.utils.toAscii('0x' + inputData);
        if (!requestId) return false;
        console.log("equest id===", requestId);
        return requestId;
    } catch (err) {
        console.log("error==", err)
    }
};

//update the status
const updateStatus = async(requestId, _payer_address, txnHash) => {
    try {
        let requestIdQuery = `SELECT id, request_id FROM payment_requests where request_id = '${requestId}'`;
        con.query(requestIdQuery, function(err, result) {
            if (err) {
                console.log(chalk.red("Error == "), err);
                return;
            }
            if (result.length == 0) {
                console.log(chalk.red("......No requestid found....."));
                return;
            }
            console.log("Result of request id===", result[0].request_id);
            console.log(chalk.green(`Updating the status to "captured" of request id = ${ result[0].request_id } with payer address = ${ _payer_address } and transaction hash = ${ txnHash }`));
            var sql = `UPDATE payments SET status = 'captured', payer_address = '${_payer_address}', bsc_txn_id = '${txnHash}'WHERE payment_request_id = '${result[0].id}'`;
            con.query(sql, function(err, resultUpdated) {
                if (err) {
                    console.log(chalk.red("Error == "), err);
                    return;
                }
                console.log(chalk.green("....Updating The DATABASE ...."));
                console.log(resultUpdated.message + chalk.green(" ..record(s) updated"));
            });
        });
    } catch (err) {
        console.log(chalk.red("Error in update status = "), err);
    }

};



//checkConnections();
fetchTransaction();
//getAllTransaction();
// setInterval(() => {
//     fetchTransaction();
// },5000)
//test();