var mysql = require('mysql');
const Web3 = require("web3");
const abiDecoder = require('abi-decoder');
const abi = require('./abi');
const axios = require('axios');
const chalk = require('chalk');
var web3 = new Web3("https://bsc-dataseed1.ninicoin.io");

//create connections
// var con = mysql.createConnection({
//     host: "bnbpay-dev.csjrjneiy6sm.ap-southeast-1.rds.amazonaws.com",
//     user: "akshay",
//     password: "at38.Bajv824djC",
//     database: "bnbpay"
// });

var con = mysql.createConnection({
    host: "db4free.net",
    user: "yudizdev",
    password: "Admin@123",
    database: "bnbpay",
    port: 3306
});

//check connections
const checkConnections = () => {
    con.connect(function(err) {
        if (err) {
            console.log("error==", err);
            return false;
        }
        console.log("Connected!");
        return true;
    });
}

//insert transaction
const fetchTransaction = async() => {
    try {
        let sql = "SELECT * from payments WHERE status = 'pending'";
        con.query(sql, function(err, txnResult) {
            if (err) throw err;
            console.log("Result==", txnResult);
            console.log("pending data == ", txnResult.length);
            if (txnResult.length == 0) {
                console.log(chalk.yellow("There are no pending transactions"));
                return;
            }
            findAllTransaction(txnResult);
        });
    } catch (err) {
        console.log("error in fetchTransaction==", err);
    }
};

const findAllTransaction = async(txnResult) => {
    let countercount = 0;
    let inner = 0;

    try {
        for (let counter of txnResult) {
            console.log("outer counter == ", countercount += 1);
            if (counter.payer_address == null) continue;
            console.log("time==", counter.updated_at);
            console.log("payer address==", counter.payer_address);
            let isoConvertTime = await getTimestamp(counter.updated_at);
            console.log("fetching timestamp==", isoConvertTime);
            let allSuccessTxn = await getAllTransaction(counter.payer_address, counter.settlement_contract_address, isoConvertTime);
            if (allSuccessTxn.length == 0) continue;
            for (let innerCounter of allSuccessTxn) {
                console.log("inner count ==", inner += 1);
                let inputData_ = innerCounter.arguments.find(o => o.argument === '_data');
                console.log("argument value==", inputData_.value);
                let requestId = await getRequestId(inputData_.value);
                if (requestId == false) {
                    console.log(chalk.red(chalk.red(`Error occured in finding request id of ${inputData_.value}`)));
                    continue;
                } else {
                    console.log("inside update....", );
                    updateStatus(requestId, counter.payer_address, innerCounter.transaction.hash);
                }

            }

        }
    } catch (err) {
        console.log(chalk.red("error in findAllTransaction===", err));
    }
};

//method for finding timestamp
const getTimestamp = async(time) => {
    try {
        var sTime = new Date(time).toLocaleString(undefined, { timeZone: 'Asia/Kolkata' });
        let seperatedTheComma = sTime.split(',');
        // console.log("seperated time====", seperatedTheComma);
        let createDate = seperatedTheComma[0].split('/');
        let createdDate = createDate[2] + '-' + createDate[1] + '-' + createDate[0] + 'T';
        let finalDate = createdDate + seperatedTheComma[1] + 'Z';
        let finalTimeStamp = finalDate.split(' ');
        // console.log("finalTimeStamp===", finalTimeStamp[0] + finalTimeStamp[1]);
        return finalTimeStamp[0] + finalTimeStamp[1];
    } catch (err) {
        console.log(chalk.red("error==", err));
    }

};

//method for getting all transaction

const getAllTransaction = async(payer_address, contract_address, timestamp) => {
    try {
        const BITQUERY_HEADER = {
            headers: {
                'Content-Type': 'application/json',
                'X-API-KEY': 'BQYZJai0Ca7mPFakurCqCOPaqi7FxlaO'
            }
        };

        let oQuery = {
            query: `{
                ethereum(network: bsc) {
                  smartContractCalls(
                    caller: {is: "${payer_address}"}
                    smartContractAddress: {is: "${contract_address}"}
                    options: {desc: "block.timestamp.iso8601"}
                    smartContractMethod: {is: "transferAndCall"}
                    date: {since: "${timestamp}"}
                  ) {
                    transaction {
                      hash
                    }
                    block {
                      timestamp {
                        iso8601
                      }
                    }
                    smartContractMethod {
                      name
                    }
                    arguments {
                      argument
                      value
                    }
                  }
                }
              }`

        };

        let _res = await axios.post('https://graphql.bitquery.io', oQuery, BITQUERY_HEADER);

        if (_res.data.data.ethereum.smartContractCalls.length == 0) {
            console.log(chalk.red("No transaction found...."))
            return _res.data.data.ethereum.smartContractCalls;
        }
        console.log(chalk.green(`All transaction of payer ${payer_address} with timestamp = "${timestamp}" ===`), _res.data.data);
        console.log(chalk.green(`Total transaction of payer ${payer_address} with timestamp ${timestamp} ===`), _res.data.data.ethereum.smartContractCalls.length);
        return _res.data.data.ethereum.smartContractCalls;
    } catch (err) {
        console.log(chalk.red("error=="), err);
    }
};

//finding request id
const getRequestId = async(inputData) => {
    try {
        let requestId = await web3.utils.toAscii('0x' + inputData);
        if (!requestId) return false;
        console.log("Request id===", requestId);
        return requestId;
    } catch (err) {
        console.log("error==", err)
    }
};

//update the status
const updateStatus = async(requestId, _payer_address, txnHash) => {
    try {
        let requestIdQuery = `SELECT id, request_id FROM payment_requests where request_id = '${requestId}'`;
        con.query(requestIdQuery, function(err, result) {
            if (err) {
                console.log(chalk.red("Error == "), err);
                return;
            }
            if (result.length == 0) {
                console.log(chalk.red("......No requestid found....."));
                return;
            }
            console.log("Result of request id===", result[0].request_id);
            console.log(chalk.green(`Updating the status to "captured" of request id = ${ result[0].request_id } with payer address = ${ _payer_address } and transaction hash = ${ txnHash }`));
            var sql = `UPDATE payments SET status = 'captured', payer_address = '${_payer_address}', bsc_txn_id = '${txnHash}'WHERE payment_request_id = '${result[0].id}'`;
            con.query(sql, function(err, resultUpdated) {
                if (err) {
                    console.log(chalk.red("Error == "), err);
                    return;
                }
                console.log(chalk.green("....Updating The DATABASE ...."));
                console.log(resultUpdated.message + chalk.green(" ..record(s) updated"));
            });
        });
    } catch (err) {
        console.log(chalk.red("Error in update status = "), err);
    }

};

//method for finding pending txn
const fetchTransactionForCheckingPendingTxn = async() => {
    try {
        //let sql = "SELECT * from payments WHERE payer_address = '0x91a6275327b42E77d24803ccC4d9682e748CEdd3' AND status = 'pending'";
        let sql = "SELECT * from payments WHERE status = 'pending'";
        con.query(sql, function(err, txnResult) {
            if (err) throw err;
            console.log("Result==", txnResult);
            console.log("pending data == ", txnResult.length);
            if (txnResult.length == 0) {
                console.log(chalk.yellow("There are no pending transactions"));
                return;
            };
            checkPendingTxn(txnResult);
        });
    } catch (err) {
        console.log("error in fetchTransaction==", err);
    }
}

//method for checking canceled txn
const checkPendingTxn = async(allPendingData) => {
    try {
        let currentTimeStamp = new Date().getTime();
        console.log("current time stamp==", currentTimeStamp);
        //console.log("all pending==", allPendingData)
        for (let counter of allPendingData) {
            if (counter.payer_address == null) continue;
            let getTime = await getTimestamp(counter.updated_at);
            let timeStampToCompare = new Date(getTime).getTime();
            console.log("time to compare===", timeStampToCompare);
            if (currentTimeStamp - timeStampToCompare > 3600) {
                console.log(`Time difference of ${currentTimeStamp} and ${timeStampToCompare}===`, currentTimeStamp - timeStampToCompare);
                //console.log("id===", counter.id);
                updateToCanceled(counter.id);
            }
        }
    } catch (err) {
        console.log(chalk.red("Error=="), err);
    }
};

//method for updating canceled txn
const updateToCanceled = (id) => {
    try {
        console.log(chalk.green(`updating the status to canceled of payments id`), id)
        let updateStatus = `UPDATE payments SET status = 'canceled' WHERE id = '${id}'`;
        con.query(updateStatus, function(err, resultUpdated) {
            if (err) {
                console.log(chalk.red("Error == "), err);
                return;
            }
            console.log(resultUpdated.message + chalk.green(" ..record(s) updated"));
        });
    } catch (err) {
        console.log(chalk.red("Error=="), err);
    }

}



checkConnections();
fetchTransaction();
fetchTransactionForCheckingPendingTxn();

// setInterval(() => {
//     fetchTransaction();
// },5000)
//test();